//
//  Constants.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 08.05.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//


// лучше для подобных случаев юзать enum
struct Constants {
    
    // CATEGORIES
    static let jsonUrlString = "https://money.yandex.ru/api/categories-list"
    static let yanCell = "YanCell"
    static let dataDownloaded = "dataDownloaded"
    static let yanTableController = "YanTableController"
    static let yanCellError = "YanCellError"
    
    // AUTHORIZATION
    static let client_id = "737ed3563d834287a4dd6b623b56e694"
    static let authorizationURL = "https://oauth.yandex.ru/authorize?response_type=token&client_id=737ed3563d834287a4dd6b623b56e694&redirect_uri=yancollection://"
    static let CallbackURL = "https://yx737ed3563d834287a4dd6b623b56e694.oauth.yandex.ru/auth/finish?platform=ios"
    
    static let appID = "890BAC9AE5B34581F442DF53003BA0102C8FA0528AD467CE6EE296CABC143F79"
    static let YMauthRequestURL = "https://money.yandex.ru/oauth/authorize?client_id=890BAC9AE5B34581F442DF53003BA0102C8FA0528AD467CE6EE296CABC143F79&response_type=code&redirect_uri=https://yx737ed3563d834287a4dd6b623b56e694.oauth.yandex.ru/auth/finish?platform=ios&scope=account-info"
    static let tokenRequestURL = "https://money.yandex.ru/oauth/token"
    static let exitRequestURL = "https://money.yandex.ru/api/revoke"
    
    static let redirected = "redirected"
    static let tokenReceived = "tokenReceived"
    static let exitComplete = "exitComplete"
    static let yandexAccessToken = "yandexAccessToken"
    
    // ACCOUNT OPENNING
    
    static let icon1Name = "icon1.png"
    static let icon2Name = "icon2.png"
    
    
}
