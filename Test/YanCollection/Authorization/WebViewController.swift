//
//  File.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 13.05.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import Foundation
import UIKit
import WebKit


final class webViewController: UIViewController {
    
    @IBOutlet var webView: WKWebView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureWebView()
    }
    
    func configureWebView() {
        webView.navigationDelegate = AuthorizationManager.shared
        let url = URL(string: Constants.YMauthRequestURL)!
        let urlRequest = URLRequest(url: url)
        webView.load(urlRequest)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(cancel), name: Notification.Name(rawValue: Constants.redirected), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: Constants.redirected), object: nil)
    }
    
    @IBAction func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    

    
}
