//
//  Errors.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 13.05.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import Foundation

enum AuthError: Error {
    case shortURL
}

