//
//  AuthorizationManager.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 13.05.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import Foundation
import WebKit
import Alamofire
import SwiftyJSON

final class AuthorizationManager: NSObject, WKNavigationDelegate {
    
    static var shared = AuthorizationManager()
    
    var access_token: String? 
    
    let redirectLength = Constants.CallbackURL.count
    
    //MARK: Functions to parse url
    private func urlToCallbackString(url: URL) -> String? {
        var urlString = url.absoluteString
        do {
            if urlString.count < redirectLength {
                throw AuthError.shortURL
            }
            let urlStringIndex = urlString.index(urlString.startIndex, offsetBy: redirectLength)
            urlString = String(urlString.prefix(upTo: urlStringIndex))
            return urlString
        } catch {
            return nil
        }
    }
    
    private func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url)?.queryItems else {
            return nil
        }
        let code = url.filter({$0.name == param}).first
        return code?.value
    }
    
    //MARK: WebView Delegate
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
        guard let url = webView.url, let urlString = urlToCallbackString(url: url) else {
            return
        }
        
        if urlString == Constants.CallbackURL {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.redirected), object: self)
            guard let code = getQueryStringParameter(url: webView.url!.absoluteString, param: "code") else {
                print("Can't get code from URL")
                return
            }
            workWithCode(code: code)
        }
    }
    
    //MARK: Get token using auth code
    private func workWithCode(code: String) {
        
        let params: [String:String] = ["code" : code,
                                       "client_id" : Constants.appID,
                                       "grant_type" : "authorization_code",
                                       "redirect_uri" : Constants.CallbackURL]
        
        request(Constants.tokenRequestURL, method: .post, parameters: params).responseJSON { (response) in
            let json = JSON(response.result.value!)
            self.access_token = json["access_token"].string
            UserDefaults.standard.set(self.access_token, forKey: Constants.yandexAccessToken)
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.tokenReceived), object: self)
        }
    }
    
    //MARK: Deauthorize
    func exit() {
        let headers: [String:String] = ["Authorization" : "Bearer \(access_token!)"]
        request(Constants.exitRequestURL, method: .post, headers: headers).response { (response) in
            let statusCode = response.response?.statusCode
            
            if statusCode == 200 {
                self.access_token = nil
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.exitComplete), object: self)
            }
        }
    }
    
}






















