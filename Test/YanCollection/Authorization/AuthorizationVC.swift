//
//  AuthorizationController.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 08.05.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import SafariServices

final class AuthorizationVC: UIViewController {
    
    @IBOutlet var label: UILabel!
    @IBOutlet var authButton: UIButton!
    @IBOutlet var exitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: Constants.tokenReceived), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: Constants.exitComplete), object: nil)
    }
    
    
    func configureViewController() {
        NotificationCenter.default.addObserver(self, selector: #selector(authorizationCompleted), name: Notification.Name(rawValue: Constants.tokenReceived), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(exitComplete), name: Notification.Name(rawValue: Constants.exitComplete), object: nil)
        
        if AuthorizationManager.shared.access_token != nil {
            authorizationCompleted()
        } else {
        exitComplete()
        label.isHidden = true
        }
    }

    @objc func authorizationCompleted() {
        label.isHidden = false
        label.text = "Authorization Complete"
        exitButton.isEnabled = true
        exitButton.backgroundColor = .red
        authButton.isEnabled = false
        authButton.backgroundColor = .gray
    }
    
    @IBAction func exit() {
        AuthorizationManager.shared.exit()
    }
    
    @objc func exitComplete() {
        label.isHidden = false
        label.text = "Exit Complete"
        exitButton.isEnabled = false
        exitButton.backgroundColor = .gray
        authButton.isEnabled = true
        authButton.backgroundColor = .blue
    }
    
    
}
