//
//  YanCategory.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 08.05.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

struct YanCategory: Decodable {
    let id: Int?
    let title: String
    let subs:[YanCategory]?
}
