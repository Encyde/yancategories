//
//  Model.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 07.05.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import UIKit


final class YandexManager {
    
    var currentCategory: [String] = [] //используется при рефреше для того, чтобы после повторной загрузки данных с сервера
                                       //в tableView отобразить именно ту вкладку, на которой был пользователь
    var handler: (([YanCategory])->Void)?
    
    func requestData() {
        
        guard let url = URL(string: Constants.jsonUrlString) else {
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else {
                return
            }
            do {
                let recievedData = try JSONDecoder().decode([YanCategory].self, from: data)
                self.handler?(recievedData)
            } catch let error    {
                print(error)
            }
        }.resume()
    }
}


















