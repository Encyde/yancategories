//
//  ViewController.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 06.05.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import UIKit

// Если ты не собираешься наследоваться от этого класа, то объяви его final
final class YanTableViewController: UITableViewController {

    let yandexManager = YandexManager()
    var tableArray: [YanCategory]?

    
    //MARK: Refresh functionality
    private func setRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl!)
        
    }
    
    @objc private func refresh() {
        
        yandexManager.handler = { [weak self] (arrayOfCategories) in
            var arr = arrayOfCategories
            for i in (self?.yandexManager.currentCategory)! {
                guard let category = arr.first(where: { $0.title == i }), let subs = category.subs else {
                    self?.tableView.refreshControl?.endRefreshing()
                    return
                }
                arr = subs
            }
            self?.tableArray = arr
            self?.tableView.reloadData()
            self?.tableView.refreshControl?.endRefreshing()
        }
        
        yandexManager.requestData()
   }
    
    //MARK: ViewController configuration
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableArray()
        setRefreshControl()
    }
    
    private func configureTableArray() {
        yandexManager.handler = { [weak self] (arrayOfCategories) in
            self?.tableArray = arrayOfCategories
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        if tableArray == nil {
            yandexManager.requestData()
            
            
        }
    }
    
    
    
    //MARK: TableView Delegtate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let tableArray = tableArray else {
            return 0
        }
        return tableArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: Constants.yanCell),
              let tableArray = tableArray
              else {
                return self.tableView.dequeueReusableCell(withIdentifier: Constants.yanCellError)!
        }
        cell.textLabel!.text = tableArray[indexPath.row].title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        guard let tableArray = tableArray, let _ = tableArray[row].subs  else {
            return
        }
    
        let controller = storyboard?.instantiateViewController(withIdentifier: Constants.yanTableController) as! YanTableViewController
        controller.tableArray = tableArray[row].subs
        
        let title = tableArray[row].title
        controller.yandexManager.currentCategory = yandexManager.currentCategory + [title]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
}
