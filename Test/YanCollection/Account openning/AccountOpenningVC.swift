//
//  AccountOpenningVC.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 07.06.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import UIKit

class AccountOpenningVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var button: UIButton!
    
    var cells: [CellData] = [CellData(cellType: CellType.AccountOpenningCell1, title: "СУММА", imageName: nil, text: nil),
                             CellData(cellType: CellType.AccountOpenningCell3, title: "Вы вносите", imageName: nil, text: "10 000 ₽"),
                             CellData(cellType: CellType.AccountOpenningCell2, title: "Минимальная сумма для \nвнесения: 500 ₽", imageName: Constants.icon1Name, text: nil),
                             CellData(cellType: CellType.AccountOpenningCell4, title: "Вы получаете", imageName: nil, text: "1 000 ₽"),
                             CellData(cellType: CellType.AccountOpenningCell4, title: "Ваша выгода", imageName: nil, text: "10 %"),
                             CellData(cellType: CellType.AccountOpenningCell1, title: "ЗАПОЛНИТЕ ДАННЫЕ КАРТЫ", imageName: nil, text: nil),
                             CellData(cellType: CellType.AccountOpenningCell5, title: "Номер карты", imageName: nil, text: ""),
                             CellData(cellType: CellType.AccountOpenningCell5, title: "Имя Владельца", imageName: nil, text: ""),
                             CellData(cellType: CellType.AccountOpenningCell5, title: "Срок действия карты", imageName: nil, text: ""),
                             CellData(cellType: CellType.AccountOpenningCell5, title: "CVC/CVV2", imageName: nil, text: ""),
                             CellData(cellType: CellType.AccountOpenningCell2, title: "Сумма будет зачислена на Ваш счет в течении 2-3 дней", imageName: Constants.icon2Name, text: nil)
    ]
    
    
    // MARK: TableView Delegate functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = cells[indexPath.row]
        
        
        switch cellData.cellType {
            
        case .AccountOpenningCell1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellData.cellType.rawValue) as! AccountOpenningCell1
            cell.label.text = cellData.title
            return cell
        case .AccountOpenningCell2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellData.cellType.rawValue) as! AccountOpenningCell2
            cell.label.text = cellData.title
            cell.icon.image = UIImage(named: cellData.imageName!)
            return cell
            
        case .AccountOpenningCell3:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellData.cellType.rawValue) as! AccountOpenningCell3
            cell.label.text = cellData.title
            cell.textField.text = cellData.text!
            
            cell.completion = { (amount) in
                var value = String(amount.dropLast())
                value = CellData.prepareSumString(value)
                self.cells[3].text = value + " ₽"
                self.cells[1].text = amount + " ₽"
                tableView.reloadData()
            }
            
            return cell
            
        case .AccountOpenningCell4:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellData.cellType.rawValue) as! AccountOpenningCell4
            cell.label1.text = cellData.title
            cell.label2.text = cellData.text
            return cell
            
        case .AccountOpenningCell5:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellData.cellType.rawValue) as! AccountOpenningCell5
            cell.label.text = cellData.title
            cell.textField.text = ""
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellData = cells[indexPath.row]
        switch cellData.cellType {
        case .AccountOpenningCell1:
            return 40
        case .AccountOpenningCell2:
            return 50
        case .AccountOpenningCell3:
            return 40
        case .AccountOpenningCell4:
            return 40
        case .AccountOpenningCell5:
            return 50
        }
    }
    
    // MARK: View setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enableHideOnTap()
        registerCells()
        button.layer.cornerRadius = 10
        
    }
    
    
    private func registerCells() {
        tableView.register(UINib(nibName: CellType.AccountOpenningCell1.rawValue, bundle: nil), forCellReuseIdentifier: CellType.AccountOpenningCell1.rawValue)
        tableView.register(UINib(nibName: CellType.AccountOpenningCell2.rawValue, bundle: nil), forCellReuseIdentifier: CellType.AccountOpenningCell2.rawValue)
        tableView.register(UINib(nibName: CellType.AccountOpenningCell3.rawValue, bundle: nil), forCellReuseIdentifier: CellType.AccountOpenningCell3.rawValue)
        tableView.register(UINib(nibName: CellType.AccountOpenningCell4.rawValue, bundle: nil), forCellReuseIdentifier: CellType.AccountOpenningCell4.rawValue)
        tableView.register(UINib(nibName: CellType.AccountOpenningCell5.rawValue, bundle: nil), forCellReuseIdentifier: CellType.AccountOpenningCell5.rawValue)
    }
    
    private func enableHideOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dissmissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dissmissKeyboard() {
        view.endEditing(true)
    }
    
}
