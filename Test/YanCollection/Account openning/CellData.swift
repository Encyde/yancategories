//
//  CellData.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 07.06.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//
import Foundation

struct CellData {
    let cellType: CellType
    let title: String
    let imageName: String?
    var text: String?
    
    init(cellType: CellType, title: String, imageName: String?, text: String?) {
        self.cellType = cellType
        self.title = title
        self.imageName = imageName
        self.text = text
    }
    
    //Разбивает числа (100000 -> 100 000)
    static func prepareSumString(_ value: String) -> String {
        let text = value.replacingOccurrences(of: " ", with: "")
        let textLength = NSString(string: text).length
        var newText = ""
        var counter = 0 - textLength % 3
        for i in text {
            if counter % 3 == 0  {
                newText.append(" \(i)")
            } else {
                newText.append(i)
            }
            counter += 1
        }
        if newText.last == " " {
            newText.removeLast()
        }
        return newText
    }
}
