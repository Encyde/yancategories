//
//  CellType.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 07.06.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

enum CellType: String {
    case AccountOpenningCell1
    case AccountOpenningCell2
    case AccountOpenningCell3
    case AccountOpenningCell4
    case AccountOpenningCell5
}
