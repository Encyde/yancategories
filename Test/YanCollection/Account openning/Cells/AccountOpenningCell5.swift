//
//  AccountOpenningCell5.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 07.06.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import UIKit

class AccountOpenningCell5: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var label: UILabel!
    @IBOutlet var textField: UITextField!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let charsAmount = NSString(string: textField.text!).length
        
        switch label.text! {
        case "Номер карты":
            
            if string == "" {
                return true
            }
            else if string >= "0" && string <= "9" {
                if charsAmount < 16 {
                return true
                }
            }
            return false
            
        case "Имя Владельца":
            
            if string == "" || string == " " {
                return true
            }
            else if string >= "А" && string <= "я" {
                return true
            } else {
                return false
            }
        case "Срок действия карты":
            
            var text = textField.text!
            if string == "" {
                if range.location == 3 {
                    text.removeLast()
                }
                textField.text = text
                return true
            }
            else if string >= "0" && string <= "9" {
                if range.location > 4 {
                    return false
                }
                if range.location == 2 {
                    text.append("/")
                }
                textField.text = text
                return true
            } else {
                return false
            }
            
            
        case "CVC/CVV2":
            
            if string == "" {
                return true
            }
            else if string >= "0" && string <= "9" {
                if charsAmount < 3 {
                    return true
                }
            }
            return false
        default:
            print("Nothing selected")
            return false
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch label.text! {
        case "Имя Владельца":
            textField.keyboardType = .default
        default:
            textField.keyboardType = .numberPad
        }
        textField.text = ""
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        var text = textField.text!
        let textLength = NSString(string: text).length
        
        switch label.text! {
            
        case "Номер карты":
            
            if textLength < 16 {
                textField.text = "Неподходящий номер карты"
            } else {
                text.insert(" ", at: text.index(text.startIndex, offsetBy: 4))
                text.insert(" ", at: text.index(text.startIndex, offsetBy: 9))
                text.insert(" ", at: text.index(text.startIndex, offsetBy: 14))
                textField.text = text
            }
        case "Срок действия карты":
            
            if textLength != 5 {
                textField.text = "Неверный срок действия карты"
            }
            
        case "CVC/CVV2":
            
            if textLength != 3 {
                textField.text = "Неверный CVC/CVV2"
            }
            
        default:
            return
        }
    }
    
}
