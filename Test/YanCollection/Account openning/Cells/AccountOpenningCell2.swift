//
//  AccountOpenningCell2.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 07.06.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import UIKit

class AccountOpenningCell2: UITableViewCell {
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var label: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
