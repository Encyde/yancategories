//
//  AccountOpenningCell3.swift
//  YanCollection
//
//  Created by Maxim Bezdenezhnykh on 07.06.2018.
//  Copyright © 2018 Maxim Bezdenezhnykh. All rights reserved.
//

import UIKit

class AccountOpenningCell3: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var label: UILabel!
    @IBOutlet var textField: UITextField!
    
    var completion: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
    }
     
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string >= "0" && string <= "9" || string == "" {
            return true
        } else {
            return false
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = textField.text!.replacingOccurrences(of: " ", with: "")
        textField.text?.removeLast()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let newText = CellData.prepareSumString(textField.text!)
        completion?(newText)
        textField.text = newText
        textField.text?.append(" ₽")
    }
    
}
